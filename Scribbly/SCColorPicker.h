//
//  SCColorPicker.h
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 SCColorPicker is a UIControl that allows a color to be selected from a 
 wheel image. 
 */
@interface SCColorPicker : UIControl

/**
 The last color selected from the color wheel. Defaults to black.
 */
@property (nonatomic, readonly, strong) UIColor *color;

/**
 The color wheel image displayed within the control.
 */
@property (nonatomic, strong) IBInspectable UIImage *colorWheel;

@end
