//
//  SCColorPicker.m
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import "SCColorPicker.h"
#import "UIView+ColorUtils.h"

@interface SCColorPicker ()

@property (nonatomic, strong) UIImageView *colorWheelView;
@property (nonatomic, readwrite, strong) UIColor *color;

@end

@implementation SCColorPicker

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configure];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure
{
    self.color = [UIColor blackColor];
    
    self.colorWheelView.image = self.colorWheel;
    self.colorWheelView.contentMode = UIViewContentModeCenter;
    [self addSubview:self.colorWheelView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.colorWheelView.frame = self.bounds;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self selectColorAtPoint:[touch locationInView:self.colorWheelView]];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    [self selectColorAtPoint:[touch locationInView:self.colorWheelView]];
}

- (void)selectColorAtPoint:(CGPoint)point
{
    self.color = [self colorAtPoint:point];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

#pragma mark - Canvas Handling

@synthesize colorWheelView = _colorWheelView;

- (UIImageView *)colorWheelView
{
    if (_colorWheelView == nil) {
        _colorWheelView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    return _colorWheelView;
}

@end
