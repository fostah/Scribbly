//
//  SCDrawableView.h
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 SCDrawableView provides a UIView that can be drawn on using the 
 moveToPoint and drawToPoint funtions.
 */
@interface SCDrawableView : UIView

/**
 Keeps track whether the drawable view has been drawn on.
 */
@property (nonatomic, readonly, getter=isDrawnOn) BOOL drawnOn;

/**
 Defines the size of the brush to be used while drawing. Defaults to 1.0.
 */
@property (nonatomic) CGFloat brushSize;

/**
 Defines the color of the brush. Defaults to black.
 */
@property (nonatomic, strong) UIColor *brushColor;

/** 
 Reference to the current position for drawing. 
 */
@property (nonatomic, readonly) CGPoint drawingPoint;

/**
 Moves the current drawing position to the provided point.
 
 @param point The new drawing location.
 */
- (void)moveToPoint:(CGPoint)point;

/**
 Draws a line from the current drawing position to the provided point.
 The provided point becomes the new drawing position.
 
 @param point The point to draw to from the current drawing position.
 */
- (void)drawToPoint:(CGPoint)point;

/**
 Handles resetting the drawable view. This includes erasing the current drawing.
 */
- (void)resetDrawing;

@end
