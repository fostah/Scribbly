//
//  SCDrawableView.m
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import "SCDrawableView.h"

static const CGFloat kDefaultBrushSize = 1.0;

@interface SCDrawableView ()

@property (nonatomic, readwrite) BOOL drawnOn;
@property (nonatomic, readwrite) CGPoint drawingPoint;

@property (nonatomic, readonly, strong) UIImageView *canvasView;

@property (nonatomic, assign) CGFloat brushRed;
@property (nonatomic, assign) CGFloat brushGreen;
@property (nonatomic, assign) CGFloat brushBlue;
@property (nonatomic, assign) CGFloat brushAlpha;

- (void)configure;

@end

@implementation SCDrawableView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configure];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure
{
    self.brushSize = kDefaultBrushSize;
    self.brushColor = [UIColor blackColor];
    
    [self addSubview:self.canvasView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.canvasView.frame = self.bounds;
}

#pragma mark - Draw Handling

- (void)setBrushColor:(UIColor *)brushColor
{
    // Break the color down into its components when it's set.
    _brushColor = brushColor;
    [_brushColor getRed:&_brushRed green:&_brushGreen blue:&_brushBlue alpha:&_brushAlpha];
}

- (void)moveToPoint:(CGPoint)point
{
    // TODO: Consider boundary conditions for drawing.
    
    self.drawingPoint = point;
}

- (void)drawToPoint:(CGPoint)point
{
    UIGraphicsBeginImageContext(self.canvasView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self.canvasView.image drawInRect:self.canvasView.bounds];
    
    // Configure drawing...
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGContextSetLineWidth(context, self.brushSize);
    CGContextSetRGBStrokeColor(context,
                               self.brushRed,
                               self.brushGreen,
                               self.brushBlue,
                               self.brushAlpha);
    
    // Define drawing...
    CGContextMoveToPoint(context, self.drawingPoint.x, self.drawingPoint.y);
    CGContextAddLineToPoint(context, point.x, point.y);

    // Render...
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    self.canvasView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    self.drawingPoint = point;
    self.drawnOn = YES;
}

- (void)resetDrawing
{
    self.canvasView.image = nil;
    self.drawnOn = NO;
}

#pragma mark - Canvas Handling

@synthesize canvasView = _canvasView;

- (UIImageView *)canvasView
{
    if (_canvasView == nil) {
        _canvasView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    return _canvasView;
}

@end
