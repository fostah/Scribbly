//
//  SCScribbleViewController.h
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SCDrawableView, SCColorPicker;


@interface SCScribbleViewController : UIViewController

/**
 The view in which the user will draw their scribbles on.
 */
@property (nonatomic, weak) IBOutlet SCDrawableView *drawableView;

/**
 When pressed, the user is prompted for whether or not they want to reset the
 drawing. Upon choosing 'Yes' the drawing is reset.
 */
@property (nonatomic, weak) IBOutlet UIButton *resetButton;

/**
 When pressed, the color picker is presented to the user.
 */
@property (nonatomic, weak) IBOutlet UIButton *selectColorButton;

/**
 Allows the user to select the color to scribble with.
 */
@property (nonatomic, weak) IBOutlet SCColorPicker *colorPicker;

/**
 Handles performing the reset action.
 
 @param sender The sender of the action.
 */
- (IBAction)resetButtonPressed:(id)sender;

/**
 Handles performing the select color action.
 
 @param sender The sender of the action.
 */
- (IBAction)selectColorButtonPressed:(id)sender;

/**
 Handles changing the color of the brush being used to draw when the color picker
 color changes.
 
 @param sender The sender of the action.
 */
- (IBAction)colorPickerValueChanged:(id)sender;

@end
