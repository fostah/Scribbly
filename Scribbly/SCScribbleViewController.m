//
//  SCScribbleViewController.m
//  Scribbly
//
//  Created by Michael Foster on 5/7/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import "SCScribbleViewController.h"
#import "SCDrawableView.h"
#import "SCColorPicker.h"

@interface SCScribbleViewController ()

@property (nonatomic) BOOL didDraw;

- (void)configureView;

- (void)showColorPicker;
- (void)hideColorPicker;

@end

@implementation SCScribbleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}

- (void)configureView
{
    self.selectColorButton.layer.borderWidth = 1.0;
    self.selectColorButton.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:1.0].CGColor;
    self.selectColorButton.layer.cornerRadius = 4.0;
    self.selectColorButton.backgroundColor = self.colorPicker.color;
    
    self.resetButton.layer.borderWidth = 1.0;
    self.resetButton.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:1.0].CGColor;
    self.resetButton.layer.cornerRadius = 4.0;
    
    self.colorPicker.hidden = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view == self.drawableView) {
        [self hideColorPicker];
        self.didDraw = NO;
        [self.drawableView moveToPoint:[touch locationInView:self.drawableView]];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view == self.drawableView) {
        self.didDraw = YES;
        [self.drawableView drawToPoint:[touch locationInView:self.drawableView]];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.didDraw) {
        UITouch *touch = [touches anyObject];
        if (touch.view == self.drawableView) {         
            [self.drawableView drawToPoint:[touch locationInView:self.drawableView]];
        }
    }
}

#pragma mark - Reset Handling

- (IBAction)resetButtonPressed:(id)sender
{
    if (!self.drawableView.isDrawnOn) {
        return;
    }
    
    // TODO: Move this text to a localizable string file.
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Reset Drawing"
                                message:@"Are you sure you want to delete this incredible scribble you've created?"
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES"
                                                        style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction *action) {
                                                          [self.drawableView resetDrawing];
                                                      }];
    [alert addAction:yesAction];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO"
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil];
    [alert addAction:noAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Color Picker Handling

- (IBAction)selectColorButtonPressed:(id)sender
{
    if (self.colorPicker.hidden) {
        [self showColorPicker];
    } else {
        [self hideColorPicker];
    }
}

- (IBAction)colorPickerValueChanged:(id)sender
{
    self.drawableView.brushColor = self.colorPicker.color;
    self.selectColorButton.backgroundColor = self.drawableView.brushColor;
}

- (void)showColorPicker
{
    self.colorPicker.alpha = 0.0;
    self.colorPicker.hidden = NO;
    [UIView animateWithDuration:0.4 animations:^{
        self.colorPicker.alpha = 1.0;
    }];
}

- (void)hideColorPicker
{
    if (self.colorPicker.hidden) {
        return;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        self.colorPicker.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.colorPicker.hidden = YES;
    }];
}

@end
