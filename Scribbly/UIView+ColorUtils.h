//
//  UIView+ColorUtils.h
//  Scribbly
//
//  Created by Michael Foster on 5/8/15.
//  Copyright (c) 2015 Fostah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ColorUtils)

/**
 Takes the given point, renders it to a context and extracts the color data
 from the rendered point.
 
 Derived from: http://stackoverflow.com/a/8354632/16524
 
 @param point The point of interest.
 
 @return The color of the point within the view.
 */
- (UIColor *)colorAtPoint:(CGPoint)point;

@end
